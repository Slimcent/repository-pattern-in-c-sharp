﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;
using BEZAO_PayDAL.Repositories;

namespace BEZAO_PayDAL.Services
{
  public  class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Register(RegisterViewModel model)
        {

            if (!Validate(model))
            {
                return;
            }

            var user = new User
            {
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                Username = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Password = model.ConfirmPassword,
                Account = new Account{AccountNumber = 1209374652},
                Created = DateTime.Now
            };
            _unitOfWork.Users.Add(user);
            _unitOfWork.Commit();
            Console.WriteLine("Add Successful!");
        }

        public void GetAllUsers()
        {
            try
            {
                var users = _unitOfWork.Users.GetAll();
                foreach (var u in users)
                {
                    Console.WriteLine(u.Name + u.Email);
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
        }

        public void Update(UpdateViewModel model)
        {
            try
            {
                var user = _unitOfWork.Users.Find(u => u.Username == model.Username);
                if (user is null) throw new Exception("User not found");
                foreach (var u in user)
                {
                    u.Email = model.Email;
                }
                _unitOfWork.Commit();
                Console.WriteLine("Update was successful");
               
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public void Login(LoginViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.UsernameEmail) || string.IsNullOrEmpty(model.Password))
                {
                    Console.WriteLine("Invalid input");
                }
                else
                {
                    model.Password = model.Password.Trim().ToLower();
                    model.UsernameEmail = model.UsernameEmail.ToLower().Trim();             
                    var isValid = LogUser(model.UsernameEmail, model.Password, out User user);
                    if (isValid)
                    {
                        Console.WriteLine($"Welcome {user.Name}");

                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                var user = _unitOfWork.Users.Get(id);
                if (user == null)
                {
                    Console.WriteLine("User not found");
                }
                else
                {
                    _unitOfWork.Users.Delete(user);
                    _unitOfWork.Commit();
                    Console.WriteLine($"{user.Name} deleted successfully");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
        }

        public void Get(int id)
        {
            try
            {
                var user = _unitOfWork.Users.Get(id);
                Console.WriteLine($"\n{user.Name}");
                if (user == null)
                {
                    Console.WriteLine("User not found");

                }
                else
                {
                    Console.WriteLine($"\n{user.Name}");
                }
            }
            catch (Exception)
            {

                throw;
            } 
        }

        private bool Validate(RegisterViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email) 
                || string.IsNullOrWhiteSpace(model.FirstName) 
                || string.IsNullOrWhiteSpace(model.LastName) 
                || (model.Birthday == new DateTime()) 
                || (string.IsNullOrWhiteSpace(model.Password))
                || (model.Password != model.ConfirmPassword))
            {
                Console.WriteLine("A field is required");
                return false;
            }

            return true;
        }

        private bool LogUser(string usernameEmail, string password, out User user)
        {
            bool isValidUser = false;
            user = null;
            var validateUser = _unitOfWork.Users.Find(u => u.Password == password &&
                                (u.Username == usernameEmail || u.Email == usernameEmail));
            foreach (var gottenUser in validateUser)
            {
                if (gottenUser != null)
                {
                    user = gottenUser;
                    isValidUser = true;
                    return isValidUser;
                } 
            }
            Console.WriteLine("Invalid User");
            return false;
        }
    }
}
