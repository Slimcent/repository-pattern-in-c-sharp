﻿using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using CodeFirstSoln.Activities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFirstSoln
{
    public static class Application
    {
        public static void AtmOptions()
        {
            Console.Clear();
            var menu = new StringBuilder();
            menu.AppendLine("Welcome to Genesys Bank, select your prefered Option\n");
            menu.Append("Press 1 to Login\n");
            menu.Append("Press 1 to Register\n");
            menu.Append("Press 2 to search User by Id\n");
            menu.Append("Press 3 to Get all Users\n");
            menu.Append("Press 4 to Update a User\n");
            menu.Append("Press 5 to Delete a User\n");
            menu.Append("Press 6 to Tranfer Money\n");
            menu.Append("Press 7 to go back to Log out\n");
            Console.WriteLine(menu.ToString());
            var selection = Console.ReadLine();

            while (selection != "1" && selection != "2" && selection != "3" && selection != "4")
            {
                Validation.InvalidPrompt(selection);
                selection = Console.ReadLine();
            }

            switch (selection)
            {
                case "1":
                    Functions.LogUser();
                    break;
                case "2":
                    Console.WriteLine("Register");
                    break;
                case "3":
                    Functions.GetById();
                    break;
                case "4":
                    Functions.GetAllUsers();
                    break;
                case "5":
                    Functions.UpdateUser();
                    break;
                case "6":
                    Functions.DeleteById();
                    break;
                case "7":
                    Exit("7");
                    break;
                default:
                    Console.WriteLine("Invalid selection made, Chose the right option");
                    break;
            }
        }

        public static void LogInOperations()
        {
            Console.Clear();
            var menu = new StringBuilder();
            menu.AppendLine("Select the Option to perform\n");
            menu.Append("Press 1 to Check Balance\n");
            menu.Append("Press 2 to Withdraw Money\n");
            menu.Append("Press 3 to Tranfer Money\n");
            menu.Append("Press 4 to Log out\n");
            Console.WriteLine(menu.ToString());
            var selection = Console.ReadLine();

            while (selection != "1" && selection != "2" && selection != "3" && selection != "4")
            {
                Validation.InvalidPrompt(selection);
                selection = Console.ReadLine();
            }

            switch (selection)
            {
                case "1":
                    Functions.GetAccount();
                    break;
                case "2":
                    Functions.Withdrawal();
                    break;
                case "3":
                    Functions.Transfer();
                    break;
                case "4":
                    AtmOptions();
                    break;
                default:
                    Console.WriteLine("Invalid selection made, Chose the right option");
                    break;
            }
        }

        public static bool Exit(string num)
        {
            return string.Equals(num, "7", StringComparison.OrdinalIgnoreCase);
        }
    }
}
