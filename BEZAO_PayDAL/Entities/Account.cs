﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BEZAO_PayDAL.Entities
{
   public class Account
    {
        public int Id { get; set; }

        [MaxLength(10)]
        [MinLength(10)]
        public int AccountNumber { get; set; }

        [Column(TypeName ="decimal(38,2)")]
        public decimal Balance { get; set; }

    }
}
