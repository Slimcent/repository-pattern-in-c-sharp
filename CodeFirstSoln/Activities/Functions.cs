﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using CodeFirstSoln.Activities;

namespace CodeFirstSoln.Activities
{
    public static class Functions
    {
        public static void EnrollUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Register(new RegisterViewModel
            {
                FirstName = "Ij",
                LastName = "Nwa",
                Email = "ijage@domain.com",
                Username = "Ij",
                Birthday = new DateTime(2000, 01, 22),
                Password = "1234@one",
                ConfirmPassword = "1234@one"
            });
        }

        public static void GetById()
        {
            var ga = new UserService(new UnitOfWork(new BezaoPayContext()));
            try
            {
                Console.WriteLine("Enter the Id to find");
                var id = Console.ReadLine();
                while (string.IsNullOrEmpty(id))
                {
                    Console.WriteLine("Invalid input");
                    id = Console.ReadLine();
                }
                ga.Get(Convert.ToInt32(id));

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public static void GetAllUsers()
        {
            var ga = new UserService(new UnitOfWork(new BezaoPayContext()));
            try
            {
                ga.GetAllUsers();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public static void DeleteById()
        {
            var ga = new UserService(new UnitOfWork(new BezaoPayContext()));
            try
            {
                Console.WriteLine("Enter the Id to Delete");
                var id = Console.ReadLine();
                while (string.IsNullOrEmpty(id))
                {
                    Console.WriteLine("Invalid input");
                    id = Console.ReadLine();
                }
                ga.Delete(Convert.ToInt32(id));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void UpdateUser()
        {
            try
            {
                var ga = new UserService(new UnitOfWork(new BezaoPayContext()));
                ga.Update(new UpdateViewModel { Username = "ij", Email = "ef@domain.com" });
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        public static void LogUser()
        {
            try
            {
                var ga = new UserService(new UnitOfWork(new BezaoPayContext()));
                ga.Login(new LoginViewModel { UsernameEmail = "ij", Password = "1234@on" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void GetAccount()
        {
            var asr = new AccountService(new UnitOfWork(new BezaoPayContext()));
            try
            {
                Console.WriteLine("Enter the Account Id to search");
                var id = Console.ReadLine();
                while (string.IsNullOrEmpty(id))
                {
                    Console.WriteLine("Invalid input");
                    id = Console.ReadLine();
                }
                asr.Get(Convert.ToInt32(id));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Deposit()
        {
            try
            {
                var dep = new TransactionService(new UnitOfWork(new BezaoPayContext()));
                dep.Deposit(new DepositViewModel { Amount = 1500, RecipientAccountNumber = 1209374652 });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Transfer()
        {
            try
            {
                var trans = new TransactionService(new UnitOfWork(new BezaoPayContext()));
                trans.Transfer(new TransferViewModel { Amount = 500, RecipientAccountNumber = 1209374652, SenderAccountNumber = 222833403 });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void Withdrawal()
        {
            try
            {
                var withd = new TransactionService(new UnitOfWork(new BezaoPayContext()));
                withd.Withdraw(new WithdrawalViewModel { Amount = 5000000, AccountNumber = 0760015555 });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
