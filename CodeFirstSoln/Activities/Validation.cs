﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeFirstSoln.Activities
{
    public static class Validation
    {
        public static void InvalidPrompt(string field)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Invalid selection, Chose the right Language Option");
            Console.ResetColor();
        }
    }
}
