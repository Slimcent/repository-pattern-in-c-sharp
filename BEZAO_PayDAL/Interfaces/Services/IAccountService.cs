﻿using BEZAO_PayDAL.Entities;

namespace BEZAO_PayDAL.Interfaces.Services
{
    public interface IAccountService
    {
        public Account Get(int userId);

    }
}