﻿using System;
using System.Collections.Generic;
using System.Text;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.UnitOfWork;
using BEZAO_PayDAL.Repositories;
using System.Linq;

namespace BEZAO_PayDAL.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TransactionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Deposit(DepositViewModel model)
        {
            try
            {
                var account = _unitOfWork.Accounts.Find(a => a.AccountNumber == model.RecipientAccountNumber).FirstOrDefault();
                if (account != null)
                {
                    if (model.Amount > 0)
                    {
                        account.Balance += model.Amount;
                        _unitOfWork.Transactions.Add(new Transaction { Amount = model.Amount, TimeStamp = DateTime.Now, TransactionMode = TransactionMode.Credit, UserId = 8 });
                        _unitOfWork.Commit();
                        Console.WriteLine("Deposit Successful");
                        Console.WriteLine($"Account Balance : {account.Balance}");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Deposit not possible");
                    }
                }
                else
                {
                    Console.WriteLine("User not found");
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        public void Transfer(TransferViewModel model)
        {
            using var context = new BezaoPayContext();
            using var transaction = context.Database.BeginTransaction();

            try
            {
                transaction.CreateSavepoint("Beginning");
                var sender = _unitOfWork.Accounts.Find(a => a.AccountNumber == model.SenderAccountNumber).FirstOrDefault();
                var receiver = _unitOfWork.Accounts.Find(a => a.AccountNumber == model.RecipientAccountNumber).FirstOrDefault();

                if (sender != null && receiver != null)
                {
                    if(model.Amount > 0)
                    {
                        sender.Balance -= model.Amount;
                        receiver.Balance += model.Amount;

                        _unitOfWork.Transactions.Add(new Transaction { Amount = model.Amount, TimeStamp = DateTime.Now, TransactionMode = TransactionMode.Credit, UserId = 8 });
                        _unitOfWork.Transactions.Add(new Transaction { Amount = model.Amount, TimeStamp = DateTime.Now, TransactionMode = TransactionMode.Debit, UserId = 2 });
                        _unitOfWork.Commit();
                        transaction.Commit();
                        Console.WriteLine("Your Transfer was Successful");
                        return;
                    }
                    else { Console.WriteLine("Transfer was not Successful"); } 
                }else { Console.WriteLine("One or two matches not found, please, Try again"); }
            }
            catch (Exception ex)
            {
                transaction.RollbackToSavepoint("Beginning");
                Console.WriteLine(ex.Message);
            }
        }

        public void Withdraw(WithdrawalViewModel model)
        {
            try
            {
                var account = _unitOfWork.Accounts.Find(a => a.AccountNumber == model.AccountNumber).FirstOrDefault();
                if (account != null)
                {
                    if (account.Balance > model.Amount)
                    {
                        account.Balance -= model.Amount;
                        _unitOfWork.Transactions.Add(new Transaction { Amount = model.Amount, TimeStamp = DateTime.Now, TransactionMode = TransactionMode.Debit, UserId = account.Id });
                        _unitOfWork.Commit();
                        Console.WriteLine("Withdrawal was successful");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Withdrawal cannot be possible");
                    }
                }
                else
                {
                    Console.WriteLine("Account number not Invalid");
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
        }
    }
}
